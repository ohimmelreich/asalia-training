# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
TrainingWeb::Application.initialize!


# global variable set to a messenger
$messengerGlobal = Messenger3.new
# constant set to a messenger
MessengerConstant = Messenger3.new
# both these version do not prevent the variable to be overwritten and cannot guarantee it is the same instance in all cases
#Rails.logger = Logger.new(STDOUT)