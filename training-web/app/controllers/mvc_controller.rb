class MvcController < ApplicationController
  def index
    
    # retrive the data
    @data = DummyData.get_data
        
    # check if there is a param asking for a specific view
    if(!params['view'].nil?)
      # if the user requests a list show the list, the default is table (index.html.erb)
      render "list" if(params['view'] == "list") # render "list" will use list.html.erb
    end
  end
  
  def list_new
    @data = DummyData.get_data    
  end
end
