class GigsController < ApplicationController
  
  # limit the response to JSON
  respond_to :json
  
  # give 
  def index
    @gigs = Gig.gigs
    respond_with @gigs
  end

  # retrieve only one item
  def show
    # pretend that the id is the index of the array...
    @gig = Gig.gigs[params['id'].to_i]
    respond_with @gig
  end
  
  # retrieve gigs by type
  def type
    @gigs = Gig.gigs
    logger.debug(params['type'])
    render json: GigList.find_by_type(params['type'])
  end
  
  # retrieve gigs by venue
  def venue
    @gigs = Gig.gigs
    logger.debug(params['venue'])
    render json: GigList.find_by_venue(params['venue'])
  end
  
end
