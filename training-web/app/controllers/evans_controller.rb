class EvansController < ApplicationController
  def index
  end
  
  def singleton
     @single = Evansauth.instance # Ruby provided Singleton instance
     
     if(!params['username'].nil? && !params['pass'].nil?)
       @user = params['username']
       @pass = params['pass']  
       
       #use singleton to authenticate and return outcome
       @outcome = @single.auth(@user, @pass)  
     end
     if(params['username'].nil?)
       @single.cleanup
     end
  end
  
  def factory
    @outcome = ''
    
    logger.debug params['next']
    
    if(!params['next'].nil?)
      
      # instantiate the object using the factory
      @outcome = EvansauthFactory.get(params['next'])

    end
  end
  
  def facade
    
  end
  
  def observer
    
  end
  
end
