class MemberController < ApplicationController
   respond_to :json
  
  # give 
  def index
    @members = Member.members
    respond_with @members
  end

  # retrieve only one item
  def show
    # pretend that the id is the index of the array...
    @member = Member.members[params['id'].to_i]
    respond_with @member
  end
  
  # retrieve  by role
  def role
    @members = Member.members
    logger.debug(params['role'])
    render json: MemberList.find_by_role(params['role'])
  end

end
