class PatternController < ApplicationController
  # the Index action, default action of the pattern controller
  def index
    # nothing to do here, just an informational page
  end
  
  # the Singleton action
  def singleton
    #the action does not do anything, the code is in the view (yes, that is bad coding!)
  end
  
  # Enum simulation in Ruby, not an pattern, but very useful
  def enum
    
  end
  
  def factory
    @outcome = '' # make sure the string is empty
    
    logger.debug params['payment']
    
    # check if there is a parameter 'payment'
    if(!params['payment'].nil?)
      
      # instantiate the object using the factory
      payment = PaymentFactory.get(params['payment'])
      
      #process the payment and return the result to the view
      @outcome = payment.process
    end
  end
  
  def facade
    # just show html
  end
  
  
  def observer
    # create new observable
    obs = ObservableClass.new("Observable", "This class notifies other classes when the description is changed")
    Rails.logger.debug "Initial observable from sender: #{obs}, #{obs.name}, #{obs.description}"

    # add listener
    obs.add_observer(NeedToKnow.new)
    obs.add_observer(AlsoNeedToKnow.new)
    
    
    # change description
    obs.description="Description changed"
    
    @result = "Check the log if the NeedToKnow was notifed"
    
  end
  
  def adapter
    @fruitList = Fruit.getAll
  end
end
