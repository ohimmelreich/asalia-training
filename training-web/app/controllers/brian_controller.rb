class BrianController < ApplicationController
  def index
    print "working controller"
  end
 
  def factory
    @outcome = '' # make sure the string is empty
    
    logger.debug params['usertype']
    
    # check if there is a parameter 'payment'
    if(!params['usertype'].nil?)
      
      # instantiate the object using the factory
      user = UserAuthFactory.get(params['usertype'])
      
      
      @outcome = user
       logger.debug(@outcome)
    end
    
  end

  def observer
    wb=Watchable.new('commence')
    logger.debug " class " + wb.to_s + " with inital value " + wb.alarm

    #listeners
    wb.add_observer(Watcherone.new)
    wb.add_observer(Watchertwo.new)

    #trigger notification by changing value of item in watchable class
    wb.trigger("second state")
  end
  
end
