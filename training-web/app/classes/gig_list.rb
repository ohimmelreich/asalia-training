class GigList
  # loading all the gigs in a instance var
  @gigs = Gig.gigs
  
  # find the gigs by type
  def self.find_by_type(type)
    # prevent .downcase on nil
    return [] if(type.nil?)
    
    @selected = []
    @gigs.each do |item|
      @selected.append(item) if(item.type.downcase == type.downcase)
    end
    return @selected
  end
  
  def self.find_by_venue(venue)
    # prevent .downcase on nil
    return [] if(venue.nil?)
    
    @selected = []
    @gigs.each do |item|
      @selected.append(item) if(item.venue.downcase == venue.downcase)
    end
    return @selected
  end

end