class MemberList
  # loading all the gigs in a instance var
  @members = Member.members
  
  # find the gigs by type
  def self.find_by_type(role)
    # prevent .downcase on nil
    return [] if(role.nil?)
    
    @selected = []
    @members.each do |item|
      @selected.append(item) if(item.role.downcase == role.downcase)
    end
    return @selected
  end
  
  # def self.find_by_venue(venue)
    # # prevent .downcase on nil
    # return [] if(venue.nil?)
#     
    # @selected = []
    # @gigs.each do |item|
      # @selected.append(item) if(item.venue.downcase == venue.downcase)
    # end
    # return @selected
  # end

end