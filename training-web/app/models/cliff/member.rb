class Member
  attr_accessor :user, :username, :role
  
  def initialize(user, username, role)
    @user = user
    @username = username
    @role = role
   
  end
  
  def self.members
    @members = []
    @members.append(Member.new('Cliff Anami', '@cliff', 'administrator'))
    @members.append(Member.new('Asilia Massive', '@asilia', 'Editor'))
    @members.append(Member.new('Asilia Jijini', '@ajijini', 'Normal User'))
    @members.append(Member.new('Asilia Zanzibar', '@azibar', 'user'))
  end
end