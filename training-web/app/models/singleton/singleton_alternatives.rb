# Messenger example using module 
module MessengerModule
  
  def self.initialize
    # no implementation
  end
   
  def self.send(message)
    # no implementation
  end
 
  def self.cleanup
    # no implementation
  end
end

# Messenger example using class methods only
class MessengerClass
  
  def self.initialize
    # no implementation
  end
   
  def self.send(message)
    # no implementation
  end
 
  def self.cleanup
    # no implementation
  end
end