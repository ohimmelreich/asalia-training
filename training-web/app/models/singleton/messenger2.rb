# Eager instantiation manual Singleton version
class Messenger2
  # property which keeps track how many messages are sent
  attr_reader :messages_sent
  
  # initialize the class instance (method called when 'new' is called)
  def initialize
    # initialize the Singleton
    @messages_sent = 0
    Rails.logger.debug "Initialize Messenger2"
    Rails.logger.debug("messages sent: #{@messages_sent}")
  end
  
  # eager because the new is called in the class, it is instantiated before code is even calling the instance method
  @@instance = Messenger2.new
   
  # method to send a message
  def send(message)
    # increment the message counter
    @messages_sent += 1
    # construct the message
    @final_message = "Messenger: #{message}"
    Rails.logger.debug(@final_message)
    # return the message
    @final_message
  end
   
  def self.instance
    return @@instance
  end
  
   # resets the message counter
  def cleanup
    @messages_sent = 0
    "Message count = 0"
  end
 
  private_class_method :new
end

