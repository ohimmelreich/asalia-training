require 'singleton' unless $test
# Lazy instantiation Singleton class using the Ruby Singleton helper
class Evansauth
  include Singleton
  # property which keeps track how many authentications are done
  attr_reader :authd_users
  
  # initialize the class instance (method called when 'new' is called)
  def initialize
    # initialize the Singleton
    @authd_users = 0
    Rails.logger.debug "Initialize Evansauth Singleton"
    Rails.logger.debug("users authenticated: #{@authd_users}")
  end
  
  # method to authenticate a user
  def auth(username, pass)
    # increment the authenticated user count
    @authd_users += 1
    
    #do actual authentication here
    secret = "asilia"
     
     if(username.match(secret))
       @outcome = 'We are Asilia'
     else
       @outcome = 'Guest'
     end
    
    # construct the message
    Rails.logger.debug(@outcome)
    # return the message
    @outcome
  end
  
  # resets the authenticated user counter
  def cleanup
    @authd_users = 0
    @outcome = "Authenticated users = 0"
   #return message
   @outcome
  end
  
end
