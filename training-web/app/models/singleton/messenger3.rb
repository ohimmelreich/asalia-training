# Eager instantiation
class Messenger3
  # property which keeps track how many messages are sent
  attr_reader :messages_sent
  
  # initialize the class instance (method called when 'new' is called)
  def initialize
    # initialize the Singleton
    @messages_sent = 0
    Rails.logger.debug "Initialize messenger"
    Rails.logger.debug("messages sent: #{@messages_sent}")
  end
  
  # method to send a message
  def send(message)
    # increment the message counter
    @messages_sent += 1
    # construct the message
    @final_message = "Messenger: #{message}"
    Rails.logger.debug(@final_message)
    # return the message
    @final_message
  end
  
  # resets the message counter
  def cleanup
    @messages_sent = 0
    "Message count = 0"
  end

end
