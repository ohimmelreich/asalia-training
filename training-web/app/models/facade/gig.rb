class Gig
  attr_accessor :artist, :venue, :date, :type
  
  def initialize(artist, venue, date, type)
    @artist = artist
    @venue = venue
    @date = date
    @type = type
  end
  
  def self.gigs
    @gigs = []
    @gigs.append(Gig.new('Greenday', 'The Temple', '01-01-2014', 'Rock'))
    @gigs.append(Gig.new('Red Hot Chili Peppers', 'Paradiso', '01-02-2014', 'Rock'))
    @gigs.append(Gig.new('Miles Davis', 'The Temple', '01-03-2014', 'Jazz'))
    @gigs.append(Gig.new('Miles Davis', 'Paradiso', '01-03-2014', 'Jazz'))
  end
end
