class DummyData
  attr_accessor :name, :description
  
  def initialize (name, description)
    @name = name
    @description = description
  end
  
  def self.get_data
    arr = []
    
    item = 20.times {|i| arr.append(DummyData.new("name #{i}", "description #{i}"))}
    return arr
  end
  
end


