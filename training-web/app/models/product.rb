class Product < ActiveRecord::Base
  
  attr_accessible :color, :description, :title
  
  validates :title, :presence => {:message => "Title is required"},
          :uniqueness => {:message => "Title already exists."},
          :length => { :maximum => 100, :message => "Must be less than 100 characters"}
  
  validates :color, :presence => {:message => "Color is required"}
  
end
