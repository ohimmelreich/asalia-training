class Fruit
  # Fruit properties
  attr_reader :name, :description, :color
  
  #create new Fruit with name, description and color
  def initialize(name, description, color)
    @name = name
    @description = description
    @color = color
  end
  
  def self.by_letter(letter)
    where("color LIKE ?", "#{letter}%").order(:color)
  end
  
  # this is an simple Facade example, get the Fruits with the colors from the database changed into a color name
  def self.getAll
    # get all the products from the database
    products = Product.all
    fruitList = []
    
    # loop through the products and assign the color
    products.each do |product|
      # create new Fruit and find the color
      f = Fruit.new(product.title, product.description, Color.find(product.color).name)
      fruitList.append(f)
    end
    # return the list of fruit 
    return fruitList
    
  end
end