class Color < ActiveRecord::Base
  attr_accessible :name
  validates :name, :presence => {:message => "Name is required"}
end
