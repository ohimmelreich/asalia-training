# This class represents a EvansAuth
class EvansAuth
  
  attr_reader :welcome_message
  
  def initialize(auth_option)
     Rails.logger.debug("auth option #{auth_option}")   
     @welcome_message = auth_option.welcome
     
  end

end