# enum class listing the auth response types
class EvansauthOptions
  include Enum
  
  EvansauthOptions.define :Asilia, "We are Asilia"
  EvansauthOptions.define :Guest, "Guest"
  EvansauthOptions.define :Asiliawelcome, "Welcome back. You\'re one of us."
  EvansauthOptions.define :Guestwelcome, "Welcome to Asilia. Pay us a visit."
  
end


