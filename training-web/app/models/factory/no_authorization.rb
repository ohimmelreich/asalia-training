# this class is used when no authorization is needed, the authorize method would be empty or simply log something
class NoAuthorization
  def authorize
    message = "No authorization needed: #{self}"
    Rails.logger.debug message
    message #return the message
  end
end