# enum class listing the payment types
class UserProviders
  include Enum
  
  UserProviders.define :Admin, "admin"
  UserProviders.define :User, "user"
  
end


