# The Payment class is created with this AbstractFactory
# The AbstractFactory decides on the payment type, at run-time which Factory classes are needed to pay
class PaymentFactory
  
  # this factory class has only one method, therefore the name 'get' is fine,
  # if the factory class would have more methods a more descriptive name would be preferred, such as get_by_type
  # you could imagine there would be other ways to decide the provider type, eg. get_by_user_profile (check below) 
  def self.get(provider_type)
    # case statement to decide which factory is needed
    case provider_type
   
      when PaymentProviders::MasterCard
        # extra initialisation can be done to make sure the class is set up correctly
        return Payment.new(MasterCardFactory.new)
   
      when PaymentProviders::VISA
        # extra initialisation can be done to make sure the class is set up correctly
        return Payment.new(VisaFactory.new)
   
      when PaymentProviders::AirTelMoney
        # extra initialisation can be done to make sure the class is set up correctly
        return Payment.new(MobileMoneyFactory.new)
   
      when PaymentProviders::Mpesa
        # extra initialisation can be done to make sure the class is set up correctly
        return Payment.new(MobileMoneyFactory.new)
      
      when PaymentProviders::PayPal
        # extra initialisation can be done to make sure the class is set up correctly
        return Payment.new(PayPalFactory.new)
      
      else
        raise "Payment provider not defined:" + (provider_type.nil? ? "nil" : provider_type)
   
    end
  end
  
  # second way of retrieving the payment type, but now by a stored preference of the user
  # calls the original factory to build the Payment with the payment provider of the user
  def self.get_by_user_profile(user_profile)
    
    # retrieve the user provider
    user_payment_provider = user_profile.payment_provider
    mobile_payment_provider = user_profile.mobile_payment_provider
    
    #if not present, return the default
    return get(DefaultProvider.new.default) if(user_payment_provider.nil? && mobile_payment_provider.nil?)
        
    # if the user has a provider use that one
    return !user_payment_provider.nil? ? get(user_payment_provider) : get(mobile_payment_provider)
  end
  
  # make the new method private to prevent creating an instance of this class
  private_class_method :new
end