# Class implementing the MasterCard payment module
class MasterCardPayment
  # the actual payment happens here
  def pay
    message = "Pay with: #{self}"
    Rails.logger.debug message
    message #return the message
  end
end