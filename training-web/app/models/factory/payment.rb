# This class represents a payment, it contains no actual code because this depends on the actual implementation of the payment
class Payment
  
  attr_reader :authorization_module, :payment_module
  
  # the class is initialized with a factory class, the Factory class then decides which authorization and payment module is used
  def initialize(payment_provider_factory)
    Rails.logger.debug("Payment provider Factory #{payment_provider_factory}")
    
    # the modules to authorize or pay might differ and the actual modules are stored in separate classes
    @authorization_module = payment_provider_factory.authorization_module
    @payment_module = payment_provider_factory.payment_module
    
    # log the module types
    Rails.logger.debug("Payment initialize #{@authorization_module} #{@payment_module}")
  end
  
  # the process method does the actual payment process
  def process
    
    # first the authorization module decides how to authorize the payment information
    auth_message = @authorization_module.authorize
    
    # the payment module would pay after authorization is successful
    payment_message = @payment_module.pay
    
    "#{auth_message} - #{payment_message}" # return the final message
    
  end

end