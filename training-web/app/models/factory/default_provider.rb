# This class represents a class which would retrieve the default payment provider from some persistent location (database?)
# and return it to the user when the default function is called
# it is however not implemented yet
class DefaultProvider
  # the not yet implemented default method
  def default
    raise "Method not implemented exception"
  end
  
end