class AdminFactory

  def initialize
  end
  
  @@adminobject=AdminFactory.new
  
  def self.instance
    return @@adminobject;
    
  end

  private_class_method :new
end
