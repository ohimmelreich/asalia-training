#Factory class for a MasterCard payment
class MasterCardFactory
  # the authorization uses the general CreditCard method
  def authorization_module
    CreditCardAuthorization.new
  end
  
  # The pay module is specific to Mastercard
  def payment_module
    MasterCardPayment.new
  end
end