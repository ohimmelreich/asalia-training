# Visa changed their authorization method from the Global provider to VISA verified
class VisaVerified
  # implement the authorization with Visa Verified
  def authorize
    message = "Authorize VISA against Visa Verified: #{self}"
    Rails.logger.debug message
    message #return the message
  end
end