class UserAuthFactory
  private_class_method :new

  def self.get(usertype)
  	
    case usertype
      when "admin"
           return AdminFactory.instance
      when "user"
          return UserFactory.new
      else
         "user not defined" + usertype
	  end
 
  end

end