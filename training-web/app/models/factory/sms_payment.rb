# this class implements the methods to authorize and pay via SMS
class SmsPayment
  
  def authorize
    message = "SMS authorization #{self}"
    Rails.logger.debug message
    message #return the message
  end
  
  def pay
    message = "Pay with: #{self}"
    Rails.logger.debug message
    message #return the message
  end
end