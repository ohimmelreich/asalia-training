# The Payment class is created with this AbstractFactory
# The AbstractFactory decides on the payment type, at run-time which Factory classes are needed to pay
class CliffFactory
  
  # this factory class has only one method, therefore the name 'get' is fine,
  # if the factory class would have more methods a more descriptive name would be preferred, such as get_by_type
  # you could imagine there would be other ways to decide the provider type, eg. get_by_user_profile (check below) 
  def self.get(provider_type)
    # case statement to decide which factory is needed
    case user_type
   
    when Username::ADMIN
        # extra initialisation can be done to make sure the class is set up correctly
        return Payment.new(MasterCardFactory.new)
   
    when Username::NORMAL
        # extra initialisation can be done to make sure the class is set up correctly
        return Payment.new(VisaFactory.new)
   
     
      else
        raise "User not defined:" + user_type
   
    end
  end
  
  # second way of retrieving the payment type, but now by a stored preference of the user
  def self.get_by_user_profile(user_profile)
    provider_type = PaymentProviders::Mpesa # would actually state: provider_type = profile.preferred_provider
    get(provider_type) # calls the original factory to build the Payment
  end
  
  # make the new method private to prevent creating an instance of this class
  private_class_method :new
end