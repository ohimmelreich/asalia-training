# enum class listing the payment types
class PaymentProviders
  include Enum
  
  PaymentProviders.define :MasterCard, "mastercard"
  PaymentProviders.define :VISA, "visa"
  PaymentProviders.define :AirTelMoney, "airtelmoney"
  PaymentProviders.define :Mpesa, "mpesa"
  PaymentProviders.define :PayPal, "paypal"
  
end


