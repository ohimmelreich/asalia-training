#Factory class to construct the mobile payment methods
class MobileMoneyFactory
  # authorize using SMS 
  def authorization_module
    NoAuthorization.new
  end
  
  # pay using SMS
  def payment_module
    SmsPayment.new
  end
end