# This Factory retrieves the correct modules for a VISA payment
class VisaFactory
  def authorization_module
    Rails.logger.debug("#{self} authorization_module CreditCardAuthorization")
    CreditCardAuthorization.new
  end
  
  def payment_module
    Rails.logger.debug("#{self} payment_module VisaPayment")
    VisaPayment.new
  end
end