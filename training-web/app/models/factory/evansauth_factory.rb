# The EvansAuth class is created with this AbstractFactory
class EvansauthFactory
   
  def self.get(auth_outcome)
    
    # case statement to decide which factory is needed
    case auth_outcome
      when EvansauthOptions::Asilia
      return EvansAuth.new(EvansAsiliaFactory.new)
   
      when EvansauthOptions::Guest
      return EvansAuth.new(EvansGuestFactory.new)
    end
  
  end
  
  # make the new method private to prevent creating an instance of this class
  private_class_method :new
end