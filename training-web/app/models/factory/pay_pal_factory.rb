class PayPalFactory
  
  def authorization_module
    PayPalAuth.new
  end
  
  def payment_module
    SmsPayment.new
  end
end