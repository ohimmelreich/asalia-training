class NeedToKnow
  def update(sender)
    Rails.logger.debug "Notified from sender: #{sender}, #{sender.name}, #{sender.description}"
  end
end