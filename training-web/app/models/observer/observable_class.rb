require 'observer'
class ObservableClass 
  include Observable
  
  attr_reader :name, :description
  
  # simple init with params
  def initialize(name, description)
    super()
    @name = name
    @description = description
  end

  # the description setter has a event handler call
  def description=(new_desc)
    @description = new_desc
    changed
    notify_observers(self)
  end
end