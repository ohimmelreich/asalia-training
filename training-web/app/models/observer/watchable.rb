require 'observer'
class Watchable
	include Observable
	attr_reader:alarm

	def initialize(alarm)
		super()
		@alarm = alarm
	end
	
	def self.trigger=(new_alarm)
		@alarm = new_alarm
		changed
		notify_observers(self)
	end
	
end