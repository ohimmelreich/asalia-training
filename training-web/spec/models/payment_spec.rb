require 'spec_helper'

describe "Create a MasterCard payment with the PaymentFactory" do
  # use the MasterCard PaymentProvider
  payment = PaymentFactory.get PaymentProviders::MasterCard 
    
  it "should be of Type Payment" do
    payment.should be_a(Payment)
  end

  it "should have a authorization module of type CreditCardAuthorization" do
    payment.authorization_module.should be_a(CreditCardAuthorization)
  end

  it "should have a payment module of type MasterCardPayment" do
    payment.payment_module.should be_a(MasterCardPayment)
  end
end

describe "Create a VISA payment with the PaymentFactory" do
  # use the VISA PaymentProvider
  payment = PaymentFactory.get PaymentProviders::VISA 
    
  it "should be of Type Payment" do
    payment.should be_a(Payment)
  end

  it "should have a authorization module of type CreditCardAuthorization" do
    payment.authorization_module.should be_a(CreditCardAuthorization)
  end

  it "should have a payment module of type VISA" do
    #payment_mc.payment_module.should be_a(MasterCardPayment)
  end
end

describe "Create a Airtel Money payment with the PaymentFactory" do
  # use the VISA PaymentProvider
  payment = PaymentFactory.get PaymentProviders::AirTelMoney 
    
  it "should be of Type Payment" do
    payment.should be_a(Payment)
  end

  it "should have a authorization module of type NoAuthorization" do
    payment.authorization_module.should be_a(NoAuthorization)
  end

  it "should have a payment module of type SmsPayment" do
    payment.payment_module.should be_a(SmsPayment)
  end
end