require 'spec_helper'

describe "Create a Payment with the PaymentFactory using the UserProfile" do
  context "with the user having a payment provider" do
    
    # mock the user profile
    before(:each) do
      # create the mock object
      @user_profile = double('user_profile')
      
      # tell the mock object what to return when the method payment_provider is called
      @user_profile.should_receive(:payment_provider){ PaymentProviders::AirTelMoney }#.with(PaymentProviders::AirTelMoney)
      @user_profile.should_receive(:mobile_payment_provider){ PaymentProviders::Mpesa }
      
      # give the mock object to the factory
      @payment = PaymentFactory.get_by_user_profile @user_profile
      
      # when the real class is used it throws an error
      #@payment = PaymentFactory.get_by_user_profile UserProfile.new
    end
      
    it "should be of Type Payment" do
      @payment.should be_a(Payment)
    end
  
    it "should have a authorization module of type NoAuthorization" do
      @payment.authorization_module.should be_a(NoAuthorization)
    end
  
    it "should have a payment module of type SmsPayment" do
      @payment.payment_module.should be_a(SmsPayment)
    end
  end
  
# in the second test we create the actual UserProfile because the class does not have that many dependencies
  context "with the user having no payment provider" do
    # mock the user profile
    before(:each) do
      # create the mock object
      @user_profile = double('user_profile')
      
      # tell the mock object what to return when the method payment_provider is called
      # in this case nil
      @user_profile.should_receive(:payment_provider)
      @user_profile.should_receive(:mobile_payment_provider)
      
      # stub the default method of the DefaultProvider to return PaymentProviders::VISA
      DefaultProvider.any_instance.stub(:default).and_return(PaymentProviders::VISA)
      
      # give the mock object to the factory
      @payment = PaymentFactory.get_by_user_profile @user_profile
  
    end
      
    it "should be of Type Payment" do
      @payment.should be_a(Payment)
    end
  
    it "should have a authorization module of type CreditCardAuthorization" do
      @payment.authorization_module.should be_a(CreditCardAuthorization)
    end
  
    it "should have a payment module of type VisaPayment" do
      @payment.payment_module.should be_a(VisaPayment)
    end
  end
  
  context "Create a Payment with the PaymentFactory using the mobile provider" do
    
    # mock the user profile
    before(:each) do
      # create the mock object
      @user_profile = double('user_profile')
      
      # tell the mock object what to return when the method payment_provider is called
      # in this case nil
      @user_profile.should_receive(:payment_provider)
      @user_profile.should_receive(:mobile_payment_provider){ PaymentProviders::Mpesa }
      
      # give the mock object to the factory
      @payment = PaymentFactory.get_by_user_profile @user_profile
  
    end
      
    it "should be of Type Payment" do
      @payment.should be_a(Payment)
    end
  
    it "should have a authorization module of type NoAuthorization" do
      @payment.authorization_module.should be_a(NoAuthorization)
    end
  
    it "should have a payment module of type SmsPayment" do
      @payment.payment_module.should be_a(SmsPayment)
  end
end
  
end

