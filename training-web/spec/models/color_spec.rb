require 'spec_helper'

describe Color do
  it "has a valid factory" do
    color = FactoryGirl.create(:color_table)
    color.should be_valid
    color.id.should be(1)
    expect(color.name).to eq("Green")
  end
  
  it "is invalid without a name" do
    FactoryGirl.build(:color_table, name: nil).should_not be_valid
  end 
end
