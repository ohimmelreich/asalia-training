require 'spec_helper'

describe Product do
  it "has a valid factory" do
    product = FactoryGirl.create(:product)
    product.should be_valid
    product.color.should be(1)
    expect(product.title).to eq("Avocado")
    expect(product.description).to eq("Good fruit")    
  end
  
  it "is invalid without a color" do
    FactoryGirl.build(:product, color: nil).should_not be_valid
  end
  
  it "is invalid without a title" do
    FactoryGirl.build(:product, title: nil).should_not be_valid
  end
  
  it "is valid without description" do
    FactoryGirl.build(:product, description: nil).should be_valid
  end 
end
