require 'spec_helper'

describe Fruit do
  before(:each) do
    green = FactoryGirl.create(:color_table, :id => 1, :name => "Green")
    orange = FactoryGirl.create(:color_table, :id => 2, :name => "Orange")
    yellow = FactoryGirl.create(:color_table, :id => 3, :name => "Yellow")
    
    apple = FactoryGirl.create(:product, :title => "Apple", :color => 1)
    apple_mango = FactoryGirl.create(:product, :title => "Apple mango", :color => 1)
    orange = FactoryGirl.create(:product, :title => "Orange", :color => 2)
    banana = FactoryGirl.create(:product, :title => "Banana", :color => 3)
  end
  
  it "returns a list of all fruits available" do
    fruits = Fruit.getAll
    fruits.size.should be(4)
  end

  it "will throw an error when there is no color matching a product" do
    blue_banana = FactoryGirl.create(:product, :title => "Blue Banana", :color => 4)
    expect{Fruit.getAll}.to raise_error
  end
end
