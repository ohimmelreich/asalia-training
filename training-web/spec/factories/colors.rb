# this prepares a default color model to use in testing
FactoryGirl.define do
  factory :color_table, class: Color do |f|
    f.id 1
    f.name "Green"
  end
end