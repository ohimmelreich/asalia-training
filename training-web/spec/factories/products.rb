# this prepares a default product model to use in testing
FactoryGirl.define do
  factory :product do |f| #factory :product_table, class: Product do |f| <- this is the same but explicitly finding the class
    f.color 1
    f.title "Avocado"
    f.description "Good fruit"
  end
end