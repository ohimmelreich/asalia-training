Given(/^the Fruit object$/) do
  # I do not need to instantiate the Fruit object
  # but I need to create the factory-girl dependent models
  green = FactoryGirl.create(:color_table, :id => 1, :name => "Green")
  orange = FactoryGirl.create(:color_table, :id => 2, :name => "Orange")
  yellow = FactoryGirl.create(:color_table, :id => 3, :name => "Yellow")
  
  apple = FactoryGirl.create(:product, :title => "Apple", :color => 1)
  apple_mango = FactoryGirl.create(:product, :title => "Apple mango", :color => 1)
  orange = FactoryGirl.create(:product, :title => "Orange", :color => 2)
  banana = FactoryGirl.create(:product, :title => "Banana", :color => 3)
end

When(/^getAll is received on self$/) do
  # get me that list of fruit
  @fruits = Fruit.getAll
end

Then(/^a list of Fruits is returned$/) do
  # check if I have the expected 4 
  @fruits.size.should be(4)
end