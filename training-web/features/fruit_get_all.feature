Feature: fruit get all

  To check if I can get all Fruit products
  As a user
  I need to be able to get all products
  
    Scenario: fruit get all
    Given the Fruit object
    When getAll is received on self
    Then a list of Fruits is returned