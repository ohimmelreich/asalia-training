require 'test_helper'
require 'user_profile'

# testing the Payment Factory class and making sure it is creating the 
# class correctly with the right auhtorization and payment module 
class PaymentFactoryTest < ActiveSupport::TestCase
  
  # Create a MasterCard payment
  test "creating a MasterCard Payment object using the Payment Factory" do
    # the PaymentProvider is given to the  Factory to decide how the class is created
    payment_mc = PaymentFactory.get PaymentProviders::MasterCard
    
    # the resulting object is a Payment object
    assert payment_mc.is_a? Payment
    # the authorization module in the Payment object should be of type CreditCardAuthorization
    assert(payment_mc.authorization_module.is_a? CreditCardAuthorization)
    # the payment module in the Payment object should be of type MasterCardPayment
    assert(payment_mc.payment_module.is_a?(MasterCardPayment))
  end
  
  # creating a visa payment class
  test "creating a VISA Payment object using the Payment Factory" do  
    payment_visa = PaymentFactory.get PaymentProviders::VISA
    
    # assert all the modules are the VISA correct modules
    assert payment_visa.is_a? Payment
    # check authorization module
    assert(payment_visa.authorization_module.is_a? CreditCardAuthorization)
    # this obviously fails: a MasterCardPayment module for a VISA Payment...
    #assert(payment_visa.payment_module.is_a?(MasterCardPayment), "Expecting a VISA payment module when paying with VISA...")
  end
  
  # creating a Airtel Money object
  test "creating a AirTel Money Payment object using the Payment Factory" do
    payment_airtel = PaymentFactory.get PaymentProviders::AirTelMoney
    
    # assert all the modules are AirTel modules 
    assert payment_airtel.is_a? Payment
    # check authorization module
    assert(payment_airtel.authorization_module.is_a? NoAuthorization)
    # check payment module
    assert(payment_airtel.payment_module.is_a?(SmsPayment))
  end
  
  #creating a Mpesa payment object
  test "creating a Mpesa Payment object using the Payment Factory" do  
    payment_airtel = PaymentFactory.get PaymentProviders::Mpesa
    
    assert payment_airtel.is_a? Payment
    # check authorization module
    assert(payment_airtel.authorization_module.is_a? NoAuthorization)
    # check payment module
    assert(payment_airtel.payment_module.is_a?(SmsPayment))
  end
  
  #creating a PayPal payment object
  test "creating a PayPal Payment object using the Payment Factory" do  
    payment_paypal = PaymentFactory.get PaymentProviders::PayPal
    
    assert payment_paypal.is_a? Payment
    # check authorization module
    assert(payment_paypal.authorization_module.is_a? PayPalAuth)
    # check payment module
    assert(payment_paypal.payment_module.is_a?(SmsPayment))
  end
  
  
end